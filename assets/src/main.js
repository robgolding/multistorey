import Vue from 'vue'
import VueMapbox from 'vue-mapbox';
import Mapbox from 'mapbox-gl';

import App from './App'

Vue.use(VueMapbox, { mapboxgl: Mapbox });

Vue.config.productionTip = false

const element = document.getElementById('app')
const app = new Vue(App).$mount(element)
