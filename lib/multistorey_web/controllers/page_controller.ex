defmodule MultiStoreyWeb.PageController do
  use MultiStoreyWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
