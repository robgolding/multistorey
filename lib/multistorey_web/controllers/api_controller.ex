defmodule MultiStoreyWeb.APIController do
  use MultiStoreyWeb, :controller

  def carparks(conn, %{"city" => city}) do
    json(conn, MultiStorey.Data.get_carparks(city))
  end
end
