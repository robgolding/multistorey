defmodule MultiStorey.Data.Nottingham do
  @url "https://geoserver.nottinghamcity.gov.uk/parking/defstatus.json"

  def text_value(element) do
    element["values"]["value"]["#text"]
  end

  def parse_definition(definition) do
    data = definition["parkingRecord"]["parkingRecord"]
    [_, name] = String.split(text_value(data["parkingName"]), ": ", [parts: 2])
    %{
      name: name,
      description: text_value(data["parkingDescription"]),
      num_spaces: data["parkingNumberOfSpaces"],
      location: [
        data["parkingLocation"]["locationForDisplay"]["longitude"],
        data["parkingLocation"]["locationForDisplay"]["latitude"],
      ],
    }
  end

  def parse_status(status) do
    data = status["parkingRecord"]["parkingRecordStatus"]
    %{
      status: data["parkingSiteOpeningStatus"],
      occupancy_status: data["parkingSiteStatus"],
      num_occupied_spaces: data["parkingOccupancy"]["parkingNumberOfOccupiedSpaces"],
    }
  end

  def parse_carpark(%{"@id" => id, "definition" => definition, "status" => status}) do
    Enum.reduce(
      [%{"id" => id}, parse_definition(definition), parse_status(status)],
      &Map.merge/2
    )
  end

  def parse_carparks(data) do
    Enum.map(data, &parse_carpark/1)
  end

  def get_carparks() do
    {:ok, resp} = HTTPoison.get @url
    {:ok, %{"carParks" => carparks}} = Poison.decode resp.body
    parse_carparks(carparks["carPark"])
  end

end
